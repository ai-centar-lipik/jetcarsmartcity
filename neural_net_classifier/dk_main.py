import os
# import glob
import pandas as pd
import xml.etree.ElementTree as ET


### params ###
# working_directory = os.getcwd()
# current_file_directory = os.path.dirname(os.path.abspath(__file__))
classifier_dir = 'neural-net-classifier'
ssd_dir = 'pytorch-ssd'
image_names_file = 'popis_slika.txt'
split_dataset = True


from utils import create_project_directories
ANNOTATION_PATH, IMAGES_PATH, TRAINVAL_PATH = create_project_directories(ssd_dir)

from utils import create_trainval
create_trainval(IMAGES_PATH, image_names_file, ANNOTATION_PATH, TRAINVAL_PATH, split_dataset = True)


