import os
import glob
from numpy.lib.function_base import diff
import pandas as pd
import xml.etree.ElementTree as ET


def create_project_directories(ssd_dir):
    working_directory = os.getcwd()
    current_file_directory = os.path.dirname(os.path.abspath(__file__))

    if str(current_file_directory).lower() != str(working_directory).lower():
        os.chdir(current_file_directory)
    ANNOTATION_PATH = os.path.join(current_file_directory, ssd_dir, 'data', 'Annotations', "")
    IMAGES_PATH = os.path.join(current_file_directory, ssd_dir, 'data', 'JPEGImages', "")
    TRAINVAL_PATH = os.path.join(current_file_directory, ssd_dir, 'data', 'ImageSets', 'Main', "")
    if not os.path.exists(ANNOTATION_PATH):
        os.makedirs(ANNOTATION_PATH)
    if not os.path.exists(IMAGES_PATH):
        os.makedirs(IMAGES_PATH)
    if not os.path.exists(TRAINVAL_PATH):
        os.makedirs(TRAINVAL_PATH)

    return ANNOTATION_PATH, IMAGES_PATH, TRAINVAL_PATH


def append_train_val(file_name, lines_list):
    with open(file_name, 'w+') as file_object:
        # 'w+' is used to create the file from beginning if it already exists
        counter = 0
        appendEOL = False
        file_object.seek(0)

        # Check if file is not empty
        # data = file_object.read(100)
        # if len(data) > 0 :
        #     appendEOL = True

        for line in lines_list:
            if appendEOL == True:
                file_object.write("\n")
            else:
                appendEOL = True
            
            file_object.write(line)
            counter += 1

    assert len(lines_list) == counter
    assert os.path.isfile(file_name) == True


def create_trainval(IMAGES_PATH, image_names_file, ANNOTATION_PATH, TRAINVAL_PATH, split_dataset = True):
    import shutil

    extensions = ['jpeg', 'jpg', 'png', 'img']
    names = []
    for f in os.listdir(IMAGES_PATH):
        suffix = f.split('.')[-1]
        if suffix in extensions:
            filename = f.split('.')[0]
            # counter = int(counter) + 1
            names.append(filename)
            # print(names) # ok

    xmls = []
    for xml_file in os.listdir(ANNOTATION_PATH):
        filename = xml_file.split('.')[0]
        xmls.append(filename)

    # intersection = set(names).intersection(xmls)
    # intersection_as_list = list(intersection)
    # print("intersection", intersection)
    difference = list(set(xmls).difference(names))
    for xml_file in os.listdir(ANNOTATION_PATH):
        filename = xml_file.split('.')[0]
        if filename in difference:
            # print("filename in difference, is not in jpeg image list", filename)
            shutil.copy(os.path.join(ANNOTATION_PATH,xml_file), os.path.join('pytorch-ssd','not_used_annotations',xml_file))
            os.remove(os.path.join(ANNOTATION_PATH, xml_file))


    difference = list(set(names).difference(xmls))
    for image in os.listdir(IMAGES_PATH):
        imagename = image.split('.')[0]
        if imagename in difference:
            # print("There is a file here", imagename)
            os.remove(os.path.join(IMAGES_PATH, image))


    new_names = []
    for f in os.listdir(IMAGES_PATH):
        suffix = f.split('.')[-1]
        if suffix in extensions:
            filename = f.split('.')[0]
            new_names.append(filename)

    if split_dataset == True:
        from sklearn.model_selection import train_test_split
        X_train, X_test, dummy_y_train, dummy_y_test = train_test_split(new_names, [1 for i in new_names], test_size=0.25, random_state=42)

    os.chdir(TRAINVAL_PATH)
    append_train_val(image_names_file, new_names)
    append_train_val("trainval.txt", X_train)
    append_train_val("test.txt", X_test)


##########################################################################

def rename_images(path):
    import os
    counter = 1
    extensions = ['jpeg', 'jpg', 'png']
    for f in os.listdir(path):
        suffix = f.split('.')[-1]
        if suffix in extensions:
            # filename = f.split('')
            new_name = '{}.{}'.format(str(counter), suffix)
            os.rename(path + f, path + new_name)
            counter = int(counter) + 1


### XML's to CSV ###
def xml_to_csv(ANNOTATION_PATH):
    # https://gist.github.com/iKhushPatel/ed1f837656b155d9b94d45b42e00f5e4
    xml_path = os.path.join(path, 'xmls', '')
    if not os.path.exists(xml_path):
        os.makedirs(xml_path)
        
    xml_list = []
    for xml_file in os.listdir(xml_path):
        tree = ET.parse(xml_path + xml_file)
        root = tree.getroot()
        for member in root.findall('object'):
            value = (root.find('filename').text,
                     int(root.find('size')[0].text),
                     int(root.find('size')[1].text),
                     member[0].text,
                     int(member[4][0].text),
                     int(member[4][1].text),
                     int(member[4][2].text),
                     int(member[4][3].text)
                     )
            xml_list.append(value)
    column_name = ['filename', 'width', 'height', 'class', 'xmin', 'ymin', 'xmax', 'ymax']
    xml_df = pd.DataFrame(xml_list, columns=column_name)

    xml_csv = xml_df.to_csv(os.path.join(ANNOTATION_PATH,'labels.csv'), index=None)

    return xml_csv
# 	'''
#     for directory in ['train','testing']:
#         image_path = os.path.join(os.getcwd(), 'images/{}'.format(directory).format(directory))
#         xml_df = xml_to_csv(image_path)
#         xml_df.to_csv('data/{}_labels.csv'.format(directory), index=None)
#         print('Successfully converted xml to csv.')
# 	''' 
# 	image_path = os.path.join(os.getcwd(), 'images/train')
# 	xml_df = xml_to_csv(image_path)
# 	xml_df.to_csv('data/train_labels.csv', index=None)

# 	image_path = os.path.join(os.getcwd(), 'images/test')
# 	xml_df = xml_to_csv(image_path)
# 	xml_df.to_csv('data/test_labels.csv',index=None)



def delete_models(model_path):
    best_loss = 10000
    for file in os.listdir(model_path):
        if not file.endswith(".pth"):
            continue
        if str(file) == 'mb2-ssd-lite-mp-0_686.pth' or str(file) == 'mobilenet-v1-ssd-mp-0_675.pth':
            continue
        try:
            loss = float(file[file.rfind("-")+1:len(file)-4])
            if loss < best_loss:
                best_loss = loss
            else:
                os.remove(os.path.join(model_path, file))

        except ValueError:
            continue
        # print('found best checkpoint with loss {:f} ({:s})'.format(best_loss, file))

# delete_models(os.path.join('pytorch-ssd', 'models'))
