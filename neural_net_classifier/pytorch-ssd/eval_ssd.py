import torch
from vision.ssd.vgg_ssd import create_vgg_ssd, create_vgg_ssd_predictor
from vision.ssd.mobilenetv1_ssd import create_mobilenetv1_ssd, create_mobilenetv1_ssd_predictor
from vision.ssd.mobilenetv1_ssd_lite import create_mobilenetv1_ssd_lite, create_mobilenetv1_ssd_lite_predictor
from vision.ssd.squeezenet_ssd_lite import create_squeezenet_ssd_lite, create_squeezenet_ssd_lite_predictor
from vision.datasets.voc_dataset import VOCDataset
from vision.datasets.open_images import OpenImagesDataset
from vision.utils import box_utils, measurements
from vision.utils.misc import str2bool, Timer
import argparse
import pathlib
import numpy as np
import logging
import sys
import os
from vision.ssd.mobilenet_v2_ssd_lite import create_mobilenetv2_ssd_lite, create_mobilenetv2_ssd_lite_predictor

import itertools
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns


def get_best_model():
    best_loss = 10000
    for file in os.listdir("models"):
        if not file.endswith(".pth"):
            continue
        if str(file) == 'mb2-ssd-lite-mp-0_686.pth' or str(file) == 'mobilenet-v1-ssd-mp-0_675.pth':
            continue
        try:
            loss = float(file[file.rfind("-")+1:len(file)-4])
            if loss < best_loss:
                best_loss = loss
            else:
                os.remove(os.path.join("models", file))

        except ValueError:
            continue

    for file in os.listdir("models"):
        if str(best_loss) in file:
            print(file)
            return os.path.join("models", file)


parser = argparse.ArgumentParser(description="SSD Evaluation on VOC Dataset.")
parser.add_argument('--net', default="mb2-ssd-lite",
                    help="The network architecture, it should be of mb1-ssd, mb1-ssd-lite, mb2-ssd-lite or vgg16-ssd.")
parser.add_argument("--trained_model", default=get_best_model(), type=str)

parser.add_argument("--dataset_type", default="voc", type=str, 
                    help='Specify dataset type. Currently support voc and open_images.')
parser.add_argument("--dataset", default="data", type=str, help="The root directory of the VOC dataset or Open Images dataset.")
parser.add_argument("--label_file", default=os.path.join('data', 'labels.txt'), type=str, help="The label file path.")
parser.add_argument("--use_cuda", type=str2bool, default=True)
parser.add_argument("--use_2007_metric", type=str2bool, default=True)
parser.add_argument("--nms_method", type=str, default="hard")
parser.add_argument("--iou_threshold", type=float, default=0.7, help="The threshold of Intersection over Union.")
parser.add_argument("--eval_dir", default="eval_results", type=str, help="The directory to store evaluation results.")
parser.add_argument('--mb2_width_mult', default=1.0, type=float,
                    help='Width Multiplifier for MobilenetV2')
args = parser.parse_args()
DEVICE = torch.device("cuda:0" if torch.cuda.is_available() and args.use_cuda else "cpu")



def group_annotation_by_class(dataset):
    true_case_stat = {}
    all_gt_boxes = {}
    all_difficult_cases = {}
    gt_classes = {}
    for i in range(len(dataset)):
        image_id, annotation = dataset.get_annotation(i)
        gt_boxes, classes, is_difficult = annotation
        # print("gt_boxes", gt_boxes) # --> [[154.   9. 160.  34.]]
        # print("classes:", classes) # --> [5] --> daje samo indekse klase iz num_classes filea za svaku sliku
        labels = classes.tolist()
        gt_classes[image_id] = labels

        gt_boxes = torch.from_numpy(gt_boxes)

        for i, difficult in enumerate(is_difficult):
            class_index = int(classes[i])
            gt_box = gt_boxes[i]
            if not difficult:
                true_case_stat[class_index] = true_case_stat.get(class_index, 0) + 1

            if class_index not in all_gt_boxes:
                all_gt_boxes[class_index] = {}
            if image_id not in all_gt_boxes[class_index]:
                all_gt_boxes[class_index][image_id] = []
            all_gt_boxes[class_index][image_id].append(gt_box)
            if class_index not in all_difficult_cases:
                all_difficult_cases[class_index]={}
            if image_id not in all_difficult_cases[class_index]:
                all_difficult_cases[class_index][image_id] = []
            all_difficult_cases[class_index][image_id].append(difficult)

    for class_index in all_gt_boxes:
        for image_id in all_gt_boxes[class_index]:
            all_gt_boxes[class_index][image_id] = torch.stack(all_gt_boxes[class_index][image_id])
    for class_index in all_difficult_cases:
        for image_id in all_difficult_cases[class_index]:
            all_gt_boxes[class_index][image_id] = torch.tensor(all_gt_boxes[class_index][image_id])

    assert len(dataset) == len(gt_classes)
    return true_case_stat, all_gt_boxes, all_difficult_cases, gt_classes


def compute_average_precision_per_class(num_true_cases, gt_boxes, difficult_cases,
                                        prediction_file, iou_threshold, use_2007_metric):
    # print("printing prediction_file in the 'compute_average_precision_per_class' function:", prediction_file)
    with open(prediction_file) as f:
        image_ids = []
        boxes = []
        scores = []
        for line in f:
            t = line.rstrip().split("\t")
            image_ids.append(t[0])
            scores.append(float(t[1]))
            box = torch.tensor([float(v) for v in t[2:]]).unsqueeze(0)
            box -= 1.0  # convert to python format where indexes start from 0
            boxes.append(box)
        scores = np.array(scores)
        sorted_indexes = np.argsort(-scores)
        boxes = [boxes[i] for i in sorted_indexes]
        image_ids = [image_ids[i] for i in sorted_indexes]
        true_positive = np.zeros(len(image_ids))
        false_positive = np.zeros(len(image_ids))
        matched = set()
        for i, image_id in enumerate(image_ids):
            box = boxes[i]
            if image_id not in gt_boxes:
                false_positive[i] = 1
                continue

            gt_box = gt_boxes[image_id]
            ious = box_utils.iou_of(box, gt_box)
            max_iou = torch.max(ious).item()
            max_arg = torch.argmax(ious).item()
            if max_iou > iou_threshold:
                if difficult_cases[image_id][max_arg] == 0:
                    if (image_id, max_arg) not in matched:
                        true_positive[i] = 1
                        matched.add((image_id, max_arg))
                    else:
                        false_positive[i] = 1
            else:
                false_positive[i] = 1

    true_positive = true_positive.cumsum()
    false_positive = false_positive.cumsum()
    precision = true_positive / (true_positive + false_positive)
    recall = true_positive / num_true_cases
    if use_2007_metric:
        return measurements.compute_voc2007_average_precision(precision, recall)
    else:
        return measurements.compute_average_precision(precision, recall)


def map_classes(class_list_file):
    if os.path.isfile(class_list_file):
        classes_map = {}
        with open(class_list_file, 'r') as infile:
            for idx,line in enumerate(infile):
                classes_map[idx+1] = line.strip()
            # prepend BACKGROUND as first class
            classes_map[0] = 'BACKGROUND'
        return classes_map


def calc_conf_matrix(prediction_dir, gt_classes, gt_boxes, class_names, iou_threshold):

    from sklearn.metrics import confusion_matrix
    nb_classes = len(class_names[1:])
    confusion_matrix = torch.zeros(nb_classes, nb_classes)

    detections = 0
    TPs = []

    for class_index, class_name in enumerate(class_names):
        # print("class_name", class_name) # --> ide redom kako je i u fileu
        if class_index == 0: continue  # ignore background
        prediction_path = prediction_dir / f"det_test_{class_name}.txt"
        with open(prediction_path, "r") as f:
            image_ids = []
            boxes = []
            scores = []
            for line in f:
                t = line.rstrip().split("\t")
                image_ids.append(t[0])
                scores.append(float(t[1]))
                box = torch.tensor([float(v) for v in t[2:]]).unsqueeze(0)
                box -= 1.0  # convert to python format where indexes start from 0
                boxes.append(box)
            assert len(image_ids) == len(boxes) == len(scores)
            
            scores = np.array(scores)
            # print("scores za svaki file u predictions directory", scores)
            # print("type(scores):", type(scores))
            # print("len(scores):", len(scores))
            # print("scores.size", scores.size)
            sorted_indexes = np.argsort(-scores)
            boxes = [boxes[i] for i in sorted_indexes]
            image_ids = [image_ids[i] for i in sorted_indexes]

            matches_per_class = set()
            for i, image_id in enumerate(image_ids):
                box = boxes[i]
                if image_id not in gt_boxes[class_index]:
                    continue
                gt_box = gt_boxes[class_index][image_id]
                ious = box_utils.iou_of(box, gt_box)
                # print("type(ious)", type(ious)) # --> <class 'torch.Tensor'>
                max_iou = torch.max(ious).item() # --> vraća vrijednost (.item ga pretvori u python broj)
                max_arg = torch.argmax(ious).item() # --> vraća index od max vrijednosti

                if max_iou > iou_threshold:
                    # taking the prediction into account
                    if (image_id, max_arg) not in matches_per_class:
                        matches_per_class.add((image_id, max_iou, max_arg))

        # print("matches:", matches_per_class) # --> only for one class label!
        ### ovo su zapravo matchevi predikcija - što točnih, što netočnih - koje su iznad default thresholda
        ### sad treba iterirati kroz označene slike i dobiti koje su GT za klase
        for img_id, labels in gt_classes.items():
            for t in matches_per_class:
                if t[0] == img_id:
                    for true_label in labels:
                        if int(true_label) == class_index:
                            TPs.append([int(true_label), img_id, t[1], class_index])
                        confusion_matrix[true_label-1, class_index-1] += 1


        detections += len(matches_per_class)
        
    # print("gt_classes", gt_classes) # --> for the entire dataset!
    gt_matches = 0
    for l in gt_classes.values():
        gt_matches += 1
    print("gt_matches:", gt_matches)
    print("detections count:", detections)
    print("len of True Positives:", len(TPs))

    return confusion_matrix



def plot_confusion_matrix(cm, class_names):
    plt.figure(figsize=(15,10))

    df_cm = pd.DataFrame(cm, index=class_names, columns=class_names).astype(int)
    heatmap = sns.heatmap(df_cm, annot=True, fmt="d")

    heatmap.yaxis.set_ticklabels(heatmap.yaxis.get_ticklabels(), rotation=0, ha='right',fontsize=15)
    heatmap.xaxis.set_ticklabels(heatmap.xaxis.get_ticklabels(), rotation=45, ha='right',fontsize=15)
    plt.ylabel('True label')
    plt.xlabel('Predicted label')
    plt.show()


if __name__ == '__main__':
    eval_path = pathlib.Path(args.eval_dir)
    eval_path.mkdir(exist_ok=True)
    timer = Timer()
    class_names = [name.strip() for name in open(args.label_file).readlines()]
    if class_names[0] != 'BACKGROUND':
        class_names.insert(0, 'BACKGROUND')

    classes_map = map_classes(args.label_file)
    print(classes_map)

    if args.dataset_type == "voc":
        dataset = VOCDataset(args.dataset, is_test=True)
    elif args.dataset_type == 'open_images':
        dataset = OpenImagesDataset(args.dataset, dataset_type="test")

    true_case_stat, all_gb_boxes, all_difficult_cases, gt_classes = group_annotation_by_class(dataset)
    # print("true_case_stat: ", true_case_stat) # --> {10: 22, 6: 68, 5: 58, 8: 26, 1: 39, 7: 31, 9: 37, 2: 23, 4: 32, 3: 36} --> koliko ima predikcija po klasi
    # print("all_gb_boxes:", all_gb_boxes) # --> za svaku klasu (key) daje opet dictionary gdje je key slika gdje se nalazi ta klasa, 
    #                                    # a vrijednosti su tensori s koordinatama gdje se ta klasa nalazi ta klasa
    # print("all_difficult_cases", all_difficult_cases) # --> ?? {10: {'12ecdea2-fb7d-11eb-aa4d-401c834115c2': [0], '53279c0a-fb7d-11eb-aa4d-401c834115c2': [0, 0], '5082f224-fb7d-11eb-aa4d-401c834115c2': [0, 0], 'd3f544c...

    if args.net == 'vgg16-ssd':
        net = create_vgg_ssd(len(class_names), is_test=True)
    elif args.net == 'mb1-ssd':
        net = create_mobilenetv1_ssd(len(class_names), is_test=True)
    elif args.net == 'mb1-ssd-lite':
        net = create_mobilenetv1_ssd_lite(len(class_names), is_test=True)
    elif args.net == 'sq-ssd-lite':
        net = create_squeezenet_ssd_lite(len(class_names), is_test=True)
    elif args.net == 'mb2-ssd-lite':
        net = create_mobilenetv2_ssd_lite(len(class_names), width_mult=args.mb2_width_mult, is_test=True)
    else:
        logging.fatal("The net type is wrong. It should be one of vgg16-ssd, mb1-ssd and mb1-ssd-lite.")
        parser.print_help(sys.stderr)
        sys.exit(1)

    timer.start("Load Model")
    net.load(args.trained_model)
    net = net.to(DEVICE)
    print(f'It took {timer.end("Load Model")} seconds to load the model.')
    if args.net == 'vgg16-ssd':
        predictor = create_vgg_ssd_predictor(net, nms_method=args.nms_method, device=DEVICE)
    elif args.net == 'mb1-ssd':
        predictor = create_mobilenetv1_ssd_predictor(net, nms_method=args.nms_method, device=DEVICE)
    elif args.net == 'mb1-ssd-lite':
        predictor = create_mobilenetv1_ssd_lite_predictor(net, nms_method=args.nms_method, device=DEVICE)
    elif args.net == 'sq-ssd-lite':
        predictor = create_squeezenet_ssd_lite_predictor(net,nms_method=args.nms_method, device=DEVICE)
    elif args.net == 'mb2-ssd-lite':
        predictor = create_mobilenetv2_ssd_lite_predictor(net, nms_method=args.nms_method, device=DEVICE)
    else:
        logging.fatal("The net type is wrong. It should be one of vgg16-ssd, mb1-ssd and mb1-ssd-lite.")
        parser.print_help(sys.stderr)
        sys.exit(1)

    results = []
    # print("len(dataset):", len(dataset)) # --> 322
    for i in range(len(dataset)):
        # print("process image", i)
        timer.start("Load Image")
        image = dataset.get_image(i)
        # print("Load Image: {:4f} seconds.".format(timer.end("Load Image")))
        timer.start("Predict")
        boxes, labels, probs = predictor.predict(image)

        # print("labels: ", labels)

        # print("Prediction: {:4f} seconds.".format(timer.end("Predict")))
        indexes = torch.ones(labels.size(0), 1, dtype=torch.float32) * i
        results.append(torch.cat([
            indexes.reshape(-1, 1),
            labels.reshape(-1, 1).float(),
            probs.reshape(-1, 1),
            boxes + 1.0  # matlab's indexes start from 1
        ], dim=1))
    results = torch.cat(results)

    # print("rezultati od model.predict objekta: ", results)
    # print(type(results)) # --> <class 'torch.Tensor'>
    # print(results.shape) # --> torch.Size([60054, 7])

    for class_index, class_name in enumerate(class_names):
        if class_index == 0: continue  # ignore background
        prediction_path = eval_path / f"det_test_{class_name}.txt"
        with open(prediction_path, "w") as f:
            sub = results[results[:, 1] == class_index, :]

            # print("type(sub): ", type(sub)) # -->  <class 'torch.Tensor'>
            # print(sub.shape) # --> ([5935, 7] --> varijabilna prva brojka, 7 uvijek druga dimenzija, 10 komada izbaci jer ima 10 klasa

            for i in range(sub.size(0)):
                prob_box = sub[i, 2:].numpy()
                image_id = dataset.ids[int(sub[i, 0])]
                print(
                    image_id + "\t" + " ".join([str(v) for v in prob_box]).replace(" ", "\t"),
                    file=f
                )

    aps = []
    print("\n\nAverage Precision Per-class:")
    for class_index, class_name in enumerate(class_names):
        if class_index == 0:
            continue
        prediction_path = eval_path / f"det_test_{class_name}.txt"
        ap = compute_average_precision_per_class(
            true_case_stat[class_index],
            all_gb_boxes[class_index],
            all_difficult_cases[class_index],
            prediction_path,
            args.iou_threshold,
            args.use_2007_metric
        )
        aps.append(ap)
        print(f"{class_name}: {ap}")

    print(f"\nAverage Precision Across All Classes: {sum(aps)/len(aps)}")

    #######################

    print("\n\nConfusion Matrix:")
    cm = calc_conf_matrix(eval_path, gt_classes, all_gb_boxes, class_names, args.iou_threshold) # args.iou_threshold=0.5
    print(cm)
    plot_confusion_matrix(cm, class_names[1:])
