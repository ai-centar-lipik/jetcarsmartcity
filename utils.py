### ### ### ### ### ### ### ### ### ### ### ### ### 
### skripta za utility funkcije
### (transformacije, directory kreiranje itd itd)
### ### ### ### ### ### ### ### ### ### ### ### ### 
import cv2

def ubij_me():
    from jetracer.nvidia_racecar import NvidiaRacecar
    car=NvidiaRacecar()
    car.steering = 0
    car.throttle = 0
    

# def get_image_sectors(frame, CROP_W_SIZE, CROP_H_SIZE):
#     height, width, channels = img.shape
#     # Number of pieces Horizontally 
#     # CROP_W_SIZE  = 3
#     # Number of pieces Vertically to each Horizontal  
#     # CROP_H_SIZE = 2

#     for ih in range(CROP_H_SIZE):
#         for iw in range(CROP_W_SIZE):
#             x = width/CROP_W_SIZE * iw 
#             y = height/CROP_H_SIZE * ih
#             h = (height / CROP_H_SIZE)
#             w = (width / CROP_W_SIZE )
#             print(x,y,h,w)
#             img = img[y:y+h, x:x+w]
# def image_sectors(frame, w_sectors):
#     height, width = frame.shape[:2]
#     w = round(width/w_sectors)
#     left_sector = frame[:, :w, :]
#     center_sector = frame[:, w:(w*2), :]
#     right_sector = frame[:, -w:, :]
#     return left_sector, center_sector, right_sector
    

    
def create_bbox(frame, left, top, right, bottom, width, height):
    box = frame[top:top+height, left:left+width, :]
    return box



def check_overlap():
    return True



def reset_counter():
    znak70 = 0
    znak90 = 0
    semafor = 0
    stop = 0
    sporedna = 0
    pjesacki = 0
    covjek = 0
    JetCar = 0
    return znak70, znak90, semafor, stop, sporedna, pjesacki, covjek, JetCar



def gstreamer_pipeline(
        capture_width=3264,
        capture_height=2464,
        display_width=500,
        display_height=300,
        framerate=21,
        flip_method=0
        ):
    return(
            "nvarguscamerasrc! "
            "video/x-raw(memory:NVMM),"
            "width=(int)%d, height=(int)%d, "
            "format=(string)NV12, framerate=(fraction)%d/1 !"
            "nvvidconv flip-method=%d ! "
            "video/x-raw, width=(int)%d, height=(int)%d, format=(string)BGRx ! "
            "videoconvert ! "
            "video/x-raw, format=(string)BGR ! appsink"
            % (
                capture_width,
                capture_height,
                framerate,
                flip_method,
                display_width,
                display_height,
                )
            )

