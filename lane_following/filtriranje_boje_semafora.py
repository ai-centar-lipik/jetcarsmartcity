import cv2
import numpy as np
from numpy.core.fromnumeric import argmax


# U funkciju ulazi roi(BoundingBox) detektiranog semafora, moram provjeriti s Dariom u kojem obliku će to biti 
  

def detect_semafor_light(roi):
    '''
    Pls ovdje samo napišite kako se računa ROI :)
    '''
    # tresholdi za crvenu i zelenu
    crvena_donja_granica = np.array([160, 90, 90], np.uint8)
    crvena_gornja_granica = np.array([180, 255, 255], np.uint8)

    zelena_donja_granica = np.array([30, 0, 0], np.uint8)
    zelena_gornja_granica = np.array([100, 255, 255], np.uint8)

    # Pravimo masku za crvenu i zelenu
    maska_crvena = cv2.inRange(roi, crvena_donja_granica, crvena_gornja_granica)
    maska_zelena = cv2.inRange(roi, zelena_donja_granica, zelena_gornja_granica)

    # Koristimo argmax za provjeru boje na semaforu i vracam 0 ako treba stati, a 1 ako vozi dalje
    if np.argmax(maska_crvena) > 0:
        return 0
    elif np.argmax(maska_zelena) > 0:
        return 1
    else:
        return -1
