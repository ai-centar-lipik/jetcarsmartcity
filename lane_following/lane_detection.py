import numpy as np
import cv2

def plotArea(image, pts):
    
    cv2.line(image, (pts[0,0], pts[0,1]), (pts[1,0],pts[1,1]), (0, 255, 0), thickness=2)
    cv2.line(image, (pts[1,0], pts[1,1]), (pts[2,0],pts[2,1]), (0, 255, 0), thickness=2)
    cv2.line(image, (pts[2,0], pts[2,1]), (pts[3,0],pts[3,1]), (0, 255, 0), thickness=2)
    cv2.line(image, (pts[3,0], pts[3,1]), (pts[0,0],pts[0,1]), (0, 255, 0), thickness=2)
    

def adaptThresh(hls_img):

    h= hls_img.shape[0]
    light = hls_img[int(h/2):h,:,1]
    # this takes the bottom half of the L channel of an HLS image


    light = light/255
    avg_light = light.sum() / (224*112)
    avg_light = round(avg_light,2)
    # this normalizes and averages the value of L channel on a frame    

    print(avg_light)
    print(-(740*avg_light - 350))
    return (740*avg_light - 350)
    


def filterByColor(image):

    imgHLS = cv2.cvtColor(image, cv2.COLOR_BGR2HLS)
    #l_value = 130 + adaptThresh(imgHLS)
    l_value = 150
    lower = np.uint8([0, l_value, 0])
    upper = np.uint8([255, 255, 255])
    #imgHLS = cv2.GaussianBlur(imgHLS,(5,5),1)
    white_mask = cv2.inRange(imgHLS, lower, upper)

   
    return white_mask


def getPeak(binary_img):
    
    one_fourth = int(binary_img.shape[1]/4)

    # zbroji vrijednosti svih stupaca prve cetvrtine slike
    left_column_sums = binary_img[:, :one_fourth].sum(axis=0)

    # zbroji vrijednosti svih stupaca zadnje cetvrtine slike
    right_column_sums = binary_img[:, one_fourth*3:].sum(axis=0)


    # nadi najvecu vrijednost zbrojenih stupaca
    # u praksi bi to trebale biti lijeva i desna linija, koje se ne bi smjele
    # nalaziti izvan prve/zadnje cetvrtine slike

    xl = np.argmax(left_column_sums)
    xr = np.argmax(right_column_sums)

    # xl bi trebao biti stupac unutar PRVE cetvrtine slike koji je sadrzavao najvise bijelih px.
    # broj tog stupca ujedno je udaljenost od LIJEVOG ruba slike

    # xr bi trebao biti stupac unutar ZADNJE cetvrtine slike koji je sadrzavao najvise bijelih px.
    # broj tog stupca ujedno je udaljenost od DESNOG ruba slike

    # zelimo li usporediti ta dva broja, potrebno je od sirine cetvrtine slike oduzeti xr
    
    xr = one_fourth - xr
    #if xr == 56:
    #    xr = 0

    return xl, xr


def putInfo(frame,txt):
    font = 0
    bl = (270,150)
    fontScale = 2
    fontColor = (0,0,255)
    lineType = 1
    
    cv2.putText(frame,txt,bl,font,fontScale,fontColor,lineType)

