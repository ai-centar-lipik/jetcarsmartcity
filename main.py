from utils import check_overlap, reset_counter, create_bbox, gstreamer_pipeline # image_sectors
import jetson.inference
import jetson.utils
import sys
sys.path.append('/home/jetson/jetracer')
from jetracer.nvidia_racecar import NvidiaRacecar
import numpy as np
import cv2

from sys import argv
import argparse


### Initialize JetCar ###
# car=NvidiaRacecar()
# throt = -0.25


### Start the camera feed ###
camera = jetson.utils.videoSource("csi://0")      # '/dev/video0' for V4L2
    ### --> camera is INPUT
display = jetson.utils.videoOutput("display://0") # 'my_video.mp4' for file
    ### --> display is OUTPUT
    
    
### SETTING THE NEURAL NET PARAMS ###
net = jetson.inference.detectNet(argv=['--model=models/mb2-ssd-lite-21sep.onnx', '--labels=models/labels.txt', '--input-blob=input_0', '--output-cvg=scores', '--output-bbox=boxes', '--threshold=0.6'])
# '--flip-method=rotate-180', 
### ### ###
### ne koristi se - nije mi upalilo s config fileom iz nekog razloga, niti arg parserom jer mreža ima '-' u parametrima
# net_config = {
#     "model": "models/mb2-ssd-lite-21sep.onnx",
#     "labels": "models/labels.txt",
#     "cvg": "scores",
#     "bbox": "boxes",
#     "threshold": 0.5
# }
# parser = argparse.ArgumentParser(description="running a re-trained ssd detectnet on a JetCar")
# parser.add_argument('--model', default="models/mb2-ssd-lite-21sep.onnx")
# parser.add_argument('--labels', default="models/labels.txt")
# parser.add_argument('--output-cvg', default="scores")
# parser.add_argument('--output-bbox', default="boxes")
# parser.add_argument('--threshold', default=0.5)
# args = parser.parse_args()
### ### ###


### Setting the LANE FOLLOWING ALGORITHM CONSTANTS ###
# Lane following system based on computer vision
# Using perspective transformation and second order lines
# Based on the LDWS authored by R.Grbic
from lane_following.filtriranje_boje_semafora import detect_semafor_light
from lane_following import lane_detection

SRC1 = np.array([
     [0, 220], #bl
     [650, 220], #br
     [485, 120], #tr
     [150, 120]], #tl
     dtype = np.float32)

DST1 = np.array([
     [0, 224], 
     [224, 224],
     [224, 0],
     [0, 0]],
     dtype = np.float32)

M = cv2.getPerspectiveTransform(SRC1, DST1)
M_inv = cv2.getPerspectiveTransform(DST1, SRC1)
# k = 0
# time = 1


### SERVER COMMUNICATION CONSTANTS ###
from server import communication

### Božo, daj se ovdje kako se nikada nisi dao




### OTHER CONSTANTS ###
horizontal_sector_count = 3


### MAIN LOOP ###
znak70 = 0
znak90 = 0
semafor = 0
stop = 0
sporedna = 0
pjesacki = 0
covjek = 0
JetCar = 0

cap = cv2.VideoCapture(gstreamer_pipeline())



while True:
    # display.SetStatus("Object Detection | Network {:.0f} FPS".format(net.GetNetworkFPS()))
    
    ### get the video signal 
    #img = camera.Capture()
    ret,frame = cap.read()
    
    img = jetson.utils.cudaFromNumpy(frame[:,:,::-1])
    print(img.shape)
    ### send the images from camera to the DetectNet model 
    detections = net.Detect(img)

    ### Start the Car 
    # car.throttle = throt
    
    
    
    
    ### CHECK INPUTS FROM SERVER ###
#     if communication.sever_input() == True:
#         continue
#     else:
#         znak70, znak90, semafor, stop, sporedna, pjesacki, covjek, JetCar = reset_counter()
    


    # print(frame.shape) # --> 720, 1280, 3
    # cv2.imshow("test", frame)
    # cv2.waitKey(1)
    height, width, channels = frame.shape
    right_sector_limits = (round((width/horizontal_sector_count)*2), width)
    middle_sector_limits = (round(width/horizontal_sector_count), round((width/horizontal_sector_count)*2))
    
    
    ### LANE FOLLOWING ALGORITHM ###  
    # napravi transformaciju perspektive 
    warped_img = cv2.warpPerspective(frame, M, (224,224), flags=cv2.INTER_LINEAR)

    # filtriranje
    white_mask = lane_detection.filterByColor(warped_img)

    # izracunaj gdje je lijeva i desna traka na slici
    left_track, right_track = lane_detection.getPeak(white_mask)
    
    #print(left_track,right_track)
    st = -(left_track - right_track)/56 -0.1
    st = round(st,2)
    # car.steering = st
    ############
    print("steering:", st)

    for detection in detections:
      #   if detection.ClassID == int(1) and detection.Left > right_sector_limits[0]:
      #       znak70 += 1
      #   if detection.ClassID == int(2) and detection.Left > right_sector_limits[0]:
      #       znak90 += 1
      #   if detection.ClassID == int(3) and detection.Left > right_sector_limits[0]:
      #       semafor += 1
      #   if detection.ClassID == int(4) and detection.Left > right_sector_limits[0]:
      #       stop += 1
      #   if detection.ClassID == int(5) and detection.Left > right_sector_limits[0]:
      #       sporedna += 1
      #   if detection.ClassID == int(6) and detection.Left > right_sector_limits[0]:
      #       pjesacki += 1
      #   if detection.ClassID == int(7) and detection.Left > middle_sector_limits[0]:
      #       covjek += 1
      #   if detection.ClassID == int(8):
      #       JetCar += 1
        
# #         print("type(detection):", type(detection), "\ndetections:", detection)
        print("ClassID:", detection.ClassID)
# #         print("Confidence:", detection.Confidence)
#         print("Levi piksel:", detection.Left)
#         print("Gornji piksel:", detection.Top)
#         print("Desni piksel:", detection.Right)
#         print("Donji piksel:", detection.Bottom)
#         print("Širina boxa:", detection.Width)
#         print("Visina boxa:", detection.Height)
# #         print("Površina boxa:", detection.Area)
# #         print("Centralni piksel:", detection.Center)
        
        
        ### TASK - napraviti funkciju koja radi ROI
        # object_bbox = create_bbox(
        #     frame,
        #     round(detection.Left),
        #     round(detection.Top),
        #     round(detection.Right),
        #     round(detection.Bottom),
        #     round(detection.Width),
        #     round(detection.Height),
        # )
        # print("object_bbox", object_bbox)
        
        
        ### LOGIC
        # 1. check if there is an overlap between the traffic signs and the right_sector
        # 2. check if there are pedestrians in the middle sector
        # 3. what to do with another JetCar???
        
        # if detection.ClassID == int(3):
        #     if semafor(object_bbox) == 0:
        #         car.throttle = 0
    
        # if detection.ClassID == int(4):
        #     car.throttle = 0
        #    break

        
    # print("checkpoint 2 - POST detections")

    # display.Render(img)
    

### ne radi jer s ctrl-c u principu ubijemo skriptu 
### trebalo bi pronaći kako ugasiti Camera feed ili Display
if not display.IsStreaming() or not camera.IsStreaming():
    from utils import ubij_me
    ubij_me()
