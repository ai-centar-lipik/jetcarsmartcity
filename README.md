# THIS IS A README FILE YOU NEED TO FOLLOW BEFORE YOU SET UP THE PROJECT LOCALLY FOR DEVELOPMENT

- How to README.me - https://www.markdownguide.org/basic-syntax/ 

---------------------------------------------------------------------------------------------------------

### Project structure
- root
    - lane following algorithm folder
        - files
    - neural net classifier folder
        - dataset
        - labelling tool ([link](https://github.com/Cartucho/OpenLabeling))
            - code used to annotate our image dataset (bounding boxes + class)
        - tensorflow-ssd-transfer-learning ([tutorial link 1](https://github.com/dusty-nv/jetson-inference/blob/master/docs/pytorch-ssd.md), [tutorial link 2](https://towardsdatascience.com/custom-object-detection-using-tensorflow-from-scratch-e61da2e10087) and **[resource](https://towardsdatascience.com/custom-object-detection-using-tensorflow-from-scratch-e61da2e10087)** )
            - code used for transfer learning (ssd-v2-mobilenet + our dataset for last layer)
        - pytorch-ssd (check https://github.com/dusty-nv/pytorch-ssd and https://github.com/qfgaohao/pytorch-ssd#download-data for tutorials)
            - requires PyTorch (env file provided within directory)
            - saves the models in .pth format
            - possible conversions to Caffe and ONNX
        - any other neural net used for transfer learning...
        - **dk_main.py**
            - creates the images, annotations and validations directories
            - fills in the data and converts to needed formats
            - only dataset (images) need to be provided
        - **utils.py** 
            - functions that the main.py file calls
    - server
        - scripts used for jetson <--> server communication
    - main.ipynb
        - a jupyter notebook with all the neccessary code to run the JetCar with all the algorithms
    - main.py 
        - bogisus koji runa sve ostalo
    - utils.py
        - utility functions to make the main.py as lightweight as possible

---------------------------------------------------------------------------------------------------------

### Environments
1. one environment file is for the LABELLING tool
    - within the 'neural-net-classifier/open-labelling' directory
    - used for annotation purposes (OpenCV version >= 3.0, numpy, tqdm and lxml)

    - installation 
        - use 'conda env create -f environment.yml' to create a new environment --> RECOMMENDED
            - the first line of the .yml file will provide the name for the new environment

2. the second env file is used to retrain a classifier neural network with a custom dataset
    - within the 'neural-net-classifier/ssd-transfer-learning' directory
    - requires tensorflow 2.6.0

3. the third environment is used for transfer learning using PyTorch
    - within 'pytorch-ssd' directory

4. env file used for the lane following algorithm
    - asdf
        - asdf
    - asdf


---------------------------------------------------------------------------------------------------------

### Pulling the code into JetCar 
1. File structure
    - asdf
    - asdf

2. Code needed for production


---------------------------------------------------------------------------------------------------------

### Dataset collection
- Using JetCar 
    - either connect JetCar to a screen OR use laptop and connect to 'jetcarIP:8888' when the JetCar and the laptop are on the same network or the JetCar connected to the laptop's hotspot'
        - use JupyterLab
        - navigate to /Documents/SmartCity/traffic-sign-classifier/
        - run file '1-data_collection_vlastiti.ipynb
        
    - the images are saved to a dataset folder (make sure the existing images are DELETED)
    - copy the images to a cloud or usb and use the Labelling tool (see below)

---------------------------------------------------------------------------------------------------------

### LABELLING PROCESS for the Custom Dataset
##### **Step by step**:

  1. Open the `main/` directory
  2. Insert the input images and videos in the folder **input/**
  3. Insert the classes in the file **class_list.txt** (one class name per line)
  4. Run the code:
  5. You can find the annotations in the folder **output/**

         python main.py [-h] [-i] [-o] [-t] [--tracker TRACKER_TYPE] [-n N_FRAMES]

         optional arguments:
          -h, --help                Show this help message and exit
          -i, --input               Path to images and videos input folder | Default: input/
          -o, --output              Path to output folder (if using the PASCAL VOC format it's important to set this path correctly) | Default: output/
          -t, --thickness           Bounding box and cross line thickness (int) | Default: -t 1
          --tracker tracker_type    tracker_type being used: ['CSRT', 'KCF','MOSSE', 'MIL', 'BOOSTING', 'MEDIANFLOW', 'TLD', 'GOTURN', 'DASIAMRPN']
          -n N_FRAMES               number of frames to track object for


##### **GUI usage**

Keyboard, press: 

<img src="https://github.com/Cartucho/OpenLabeling/blob/master/keyboard_usage.jpg">

| Key | Description |
| --- | --- |
| a/d | previous/next image |
| s/w | previous/next class |
| e | edges |
| h | help |
| q | quit |

Video:

| Key | Description |
| --- | --- |
| p | predict the next frames' labels |

Mouse:
  - Use two separate left clicks to do each bounding box
  - **Right-click** -> **quick delete**!
  - Use the middle mouse to zoom in and out
  - Use double click to select a bounding box


**after finishing and copying the annotations, DELETE the images from input and annotations from output**

---------------------------------------------------------------------------------------------------------

### ReTraining with PyTorch SSD
1. Navigate to /neural-net-classifier/pytorch-ssd/
2. Create environment from environment.yml
3. Activate environment 
4. Copy the images into /data/JPEGImages/
5. Copy the OpenLabelling annotations (XML's) from /neural-net-classifier/open-labelling/main/output/PASCAL_VOC/ to /data/Annotations/
6. Run file dk_main.py
---
7. Run train_ssd.py (check parameters first)
